import { Controller, Get, Inject, UseInterceptors } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AppService } from './app.service';
import { TransformInterceptor } from './common/utils/interpretor/transform-interpretor';

@UseInterceptors(new TransformInterceptor())
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject(ConfigService)
    private configService: ConfigService,
  ) {}

  @Get()
  async getHello() {
    return this.appService.getHello();
  }
}
