import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PaginateRequestDTO } from '../../constant/share.dto';

export interface PaginateResponse<T> {
  data: T;
  paginate: PaginateItemResponseDTO;
  success: boolean;
  status: number;
  message: string;
}

export interface PaginateItemResponseDTO {
  page: number;
  limit: number;
  total_rows: number;
  total_pages: number;
}

export interface PaginateDataResponDTO<T> {
  result: T[];
  paginate: PaginateItemResponseDTO;
}

export const generatePaginateResponse = (
  query: PaginateRequestDTO,
  total: number,
): PaginateItemResponseDTO => {
  const total_pages = total / Number(query?.limit || 1);
  return {
    limit: Number(query?.limit || 10),
    page: Number(query?.page || 1),
    total_rows: total,
    total_pages: Math.ceil(total_pages),
  };
};

@Injectable()
export class PaginateTransformInterceptor<T>
  implements NestInterceptor<T, PaginateResponse<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<PaginateResponse<T>> {
    return next.handle().pipe(
      map(
        (data: PaginateDataResponDTO<T>) =>
          ({
            status: context.switchToHttp().getResponse().statusCode,
            success: true,
            message: 'OK',
            data: data.result,
            paginate: data.paginate,
          } as PaginateResponse<T>),
      ),
    );
  }
}
