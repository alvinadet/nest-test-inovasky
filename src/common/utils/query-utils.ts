import { FindManyOptions, Like } from 'typeorm';
import { PaginateRequestDTO } from '../constant/share.dto';

export const filterQuery = (
  columns: string[],

  query: PaginateRequestDTO,
): FindManyOptions => {
  const take = query?.limit ? Number(query?.limit) : 10;
  const skip = query?.page ? Number(query?.page - 1) : 0;
  const search = query?.keyword || '';

  const filters = [];

  let order = {};

  order = Object.assign(
    order,
    query?.sort_by
      ? { [query?.sort_by]: query?.sort_dir ? query?.sort_dir : 'ASC' }
      : {},
  );
  if (search?.length > 0) {
    for (const c of columns) {
      filters.push(Object.assign({}, { [c]: Like('%' + search + '%') }));
    }
  }

  return {
    where: filters,
    order,
    take,
    skip,
  };
};
