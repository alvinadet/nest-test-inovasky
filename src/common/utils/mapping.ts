import { StatusTransactionENUM } from '../constant/status-transaction';

export const mapStatusTransactionToData = (payload: StatusTransactionENUM) => {
  switch (payload) {
    case StatusTransactionENUM.PENDING:
      return 'PENDING';

    case StatusTransactionENUM.SUCCESS:
      return 'SUCCESS';

    default:
      return 'FAILED';
  }
};
