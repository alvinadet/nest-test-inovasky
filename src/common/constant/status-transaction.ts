export enum StatusTransactionENUM {
  PENDING = 0,
  SUCCESS = 1,
  FAILED = 2,
}

export enum StatusTransactionCallbackENUM {
  SUCCESS = 1,
  FAILED = 2,
}
