export interface PaginateRequestDTO {
  keyword: string;
  sort_by: string;
  sort_dir: string;
  page: number;
  limit: number;
}
