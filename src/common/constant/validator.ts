import { IsUUID } from 'class-validator';

export class UUIDValidatorDTO {
  @IsUUID()
  guid: string;
}
