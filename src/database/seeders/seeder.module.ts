import { Logger, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import configuration from '../../common/config/configuration';
import { enviroments } from '../../enviroments';
import { DatabaseModule } from '../database.module';
import { MemberSeederModule } from './member-type/member-type.module';
import { Seeder } from './seeder.service';

@Module({
  imports: [
    DatabaseModule,
    MemberSeederModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: enviroments[process.env.NODE_ENV] || '.env',
      load: [configuration],
    }),
  ],
  providers: [Logger, Seeder],
})
export class SeederModule {}
