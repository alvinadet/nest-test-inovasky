import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MemberType } from '../../../member-type/entity/member-type.entity';
import { Member } from '../../../member/entity/member.entity';
import { MemberSeederService } from './member-type-service';

@Module({
  imports: [TypeOrmModule.forFeature([MemberType, Member])],
  providers: [MemberSeederService],
  exports: [MemberSeederService],
})
export class MemberSeederModule {}
