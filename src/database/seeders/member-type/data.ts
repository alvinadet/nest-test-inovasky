import { MemberType } from '../../../member-type/entity/member-type.entity';
import { Member } from '../../../member/entity/member.entity';

export const memberTypes: Partial<MemberType>[] = [
  { name: 'default' },
  { name: 'vip' },
];

export const members: Partial<Member>[] = [
  {
    name: 'user1',
    balance: 1000,
    email: 'user1@mail.com',
    password: 'foo',
    photo_profile: 'http://user2.com',
    phone_number: '0897748',
    member_type_guid: null,
  },
  {
    name: 'user2',
    balance: 500,
    email: 'user2@mail.com',
    password: 'foo',
    photo_profile: 'http://user2.com',
    phone_number: '0898444',
    member_type_guid: null,
  },
];
