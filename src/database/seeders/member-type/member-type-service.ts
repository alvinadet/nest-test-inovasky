import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MemberType } from '../../../member-type/entity/member-type.entity';
import { Member } from '../../../member/entity/member.entity';
import { members, memberTypes } from './data';

/**
 * Service dealing with language based operations.
 *
 * @class
 */
@Injectable()
export class MemberSeederService {
  /**
   * Create an instance of class.
   *
   * @constructs
   *
   * @param {Repository<MemberType>} memberTypeRepo
   */
  constructor(
    @InjectRepository(MemberType)
    private readonly memberTypeRepo: Repository<MemberType>,

    @InjectRepository(Member)
    private readonly memberRepo: Repository<Member>,
  ) {}
  /**
   * Seed all member-type.
   *
   * @function
   */
  async createMemberType(): Promise<Array<MemberType>> {
    const memberTypesColections: MemberType[] = [];
    for (const memberType of memberTypes) {
      await this.memberTypeRepo
        .findOne({ name: memberType.name })
        .then(async (dbLangauge) => {
          if (dbLangauge) {
            return Promise.resolve(null);
          }
          memberTypesColections.push(
            await this.memberTypeRepo.save(memberType),
          );
        })
        .catch((error) => {
          throw error;
        });
      return memberTypesColections;
    }
  }

  async createMember(): Promise<Array<Member>> {
    const memberCollections: Member[] = [];
    for (const member of members) {
      await this.memberRepo
        .findOne({ name: member.name })
        .then(async (dbLangauge) => {
          if (dbLangauge) {
            return Promise.resolve(null);
          }
          const memberType = await this.memberTypeRepo.manager
            .getRepository(MemberType)
            .findOne({ where: { name: memberTypes[0].name } });
          const resData = await this.memberRepo.save({
            ...member,
            member_type_guid: memberType?.GUID,
          });

          memberCollections.push(resData);
        })
        .catch((error) => Promise.reject(error));
    }

    return memberCollections;
  }
}
