import { Injectable, Logger } from '@nestjs/common';
import { MemberSeederService } from './member-type/member-type-service';

@Injectable()
export class Seeder {
  constructor(
    private readonly logger: Logger,
    private readonly memberTypeSeederService: MemberSeederService,
  ) {}
  async seed() {
    await this.memberType()
      .then((completed) => {
        this.logger.debug('Successfuly completed seeding member type...');
        Promise.resolve(completed);
      })
      .catch((error) => {
        this.logger.error('Failed seeding member type...');
        Promise.reject(error);
      });

    await this.member()
      .then((completed) => {
        this.logger.debug('Successfuly completed seeding member...');
        Promise.resolve(completed);
      })
      .catch((error) => {
        this.logger.error('Failed seeding member...');
        Promise.reject(error);
      });
  }

  async memberType() {
    return await this.memberTypeSeederService
      .createMemberType()
      .then((createdLanguages) => {
        // Can also use this.logger.verbose('...');
        this.logger.debug(
          'No. of member type created : ' +
            // Remove all null values and return only created languages.
            createdLanguages.filter(
              (nullValueOrCreatedLanguage) => nullValueOrCreatedLanguage,
            ).length,
        );
        return Promise.resolve(true);
      })
      .catch((error) => Promise.reject(error));
  }

  async member() {
    return await this.memberTypeSeederService
      .createMember()
      .then((createdLanguages) => {
        // Can also use this.logger.verbose('...');
        this.logger.debug(
          'No. of member created : ' +
            // Remove all null values and return only created languages.
            createdLanguages.filter(
              (nullValueOrCreatedLanguage) => nullValueOrCreatedLanguage,
            ).length,
        );
        return Promise.resolve(true);
      })
      .catch((error) => Promise.reject(error));
  }
}
