import { StatusTransactionENUM } from '../../common/constant/status-transaction';
import { Log } from '../../log/entity/log.entity';
import { TransactionDTO } from '../dto/transaction.dto';

export const transactionTransformer = (model: Log): TransactionDTO => {
  return {
    created_at: model.created_at,
    updated_at: model.updated_at,
    amount: Number(model.amount),
    description: model.description,
    fee: Number(model.fee),
    member_guid: model.order.member_guid,
    order_guid: model.order_guid,
    product_code: model.product_code,
    product_name: model.product_name,
    status: Number(model.status) as StatusTransactionENUM,
    total_amount: Number(model.total_amount),
    transaction_id: model.GUID,
  };
};
