import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { PaginateRequestDTO } from '../common/constant/share.dto';
import {
  generatePaginateResponse,
  PaginateDataResponDTO,
} from '../common/utils/interpretor/paginate-transform-interpretor';
import { filterQuery } from '../common/utils/query-utils';
import { Log } from '../log/entity/log.entity';
import { TransactionDTO } from './dto/transaction.dto';
import { transactionTransformer } from './transformer/transaction.transformer';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(Log)
    private logRepository: Repository<Log>,
  ) {}

  async findAll(
    query: PaginateRequestDTO,
  ): Promise<PaginateDataResponDTO<TransactionDTO>> {
    const filter = filterQuery(['product_name', 'product_code'], query);

    const [result, total] = await this.logRepository.findAndCount(
      Object.assign(filter, {
        relations: ['order'],
      } as FindManyOptions),
    );

    return {
      result: result.map(transactionTransformer),
      paginate: generatePaginateResponse(query, total),
    };
  }
}
