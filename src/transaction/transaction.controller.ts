import {
  Controller,
  Get,
  Inject,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { PaginateRequestDTO } from '../common/constant/share.dto';
import { PaginateTransformInterceptor } from '../common/utils/interpretor/paginate-transform-interpretor';
import { TransactionService } from './transaction.service';

@Controller('transaction')
export class TransactionController {
  constructor(
    @Inject(TransactionService)
    private transactionService: TransactionService,
  ) {}

  @UseInterceptors(new PaginateTransformInterceptor())
  @Get()
  async findAll(@Query() query?: PaginateRequestDTO) {
    return await this.transactionService.findAll(query);
  }
}
