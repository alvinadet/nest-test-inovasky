import { StatusTransactionENUM } from '../../common/constant/status-transaction';

export class TransactionDTO {
  order_guid: string;
  transaction_id: string;
  member_guid: string;
  product_code: string;
  product_name: string;
  fee: number;
  amount: number;
  total_amount: number;
  description: string;
  status: StatusTransactionENUM;
  updated_at: Date;
  created_at: Date;
}
