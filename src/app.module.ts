import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import configuration from './common/config/configuration';
import { DatabaseModule } from './database/database.module';
import { SeederModule } from './database/seeders/seeder.module';
import { enviroments } from './enviroments';
import { LogModule } from './log/log.module';
import { MemberTypeModule } from './member-type/member-type.module';
import { MemberModule } from './member/member.module';
import { OrderModule } from './order/order.module';
import { SubscriberModule } from './subscriber/subscriber.module';
import { TransactionCallbackModule } from './transaction-callback/transaction-callback.module';
import { TransactionModule } from './transaction/transaction.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: enviroments[process.env.NODE_ENV] || '.env',
      load: [configuration],
    }),
    DatabaseModule,
    MemberModule,
    SubscriberModule,
    MemberTypeModule,
    OrderModule,
    LogModule,
    TransactionCallbackModule,
    TransactionModule,
    SeederModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
