import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'member_type',
})
export class MemberType {
  @PrimaryGeneratedColumn('uuid', { name: 'guid' })
  GUID: string;

  @Column({ name: 'name', type: 'varchar' })
  name: string;

  @Column({ name: 'created_at', nullable: true, default: new Date() })
  createdAt: Date;

  @Column({ name: 'updated_at', nullable: true, default: new Date() })
  updatedAt: Date;
}
