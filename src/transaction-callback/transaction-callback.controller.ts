import {
  Body,
  Controller,
  Inject,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { TransformInterceptor } from '../common/utils/interpretor/transform-interpretor';
import { TransactionCallbackDTO } from './dto/transaction-callback.dto';
import { TransactionCallbackService } from './transaction-callback.service';

@UseInterceptors(new TransformInterceptor())
@Controller('transaction-callback')
export class TransactionCallbackController {
  constructor(
    @Inject(TransactionCallbackService)
    private transactionCallbackService: TransactionCallbackService,
  ) {}

  @Post()
  async updateTransaction(@Body() payload: TransactionCallbackDTO) {
    return await this.transactionCallbackService.updateTransaction(payload);
  }
}
