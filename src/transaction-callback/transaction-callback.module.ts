import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Log } from '../log/entity/log.entity';
import { TransactionCallbackService } from './transaction-callback.service';
import { TransactionCallbackController } from './transaction-callback.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Log])],
  providers: [TransactionCallbackService],
  controllers: [TransactionCallbackController],
})
export class TransactionCallbackModule {}
