import { IsEnum, IsUUID } from 'class-validator';
import { StatusTransactionCallbackENUM } from '../../common/constant/status-transaction';

export class TransactionCallbackDTO {
  @IsUUID()
  transaction_id: string;

  @IsEnum(StatusTransactionCallbackENUM)
  status: StatusTransactionCallbackENUM;
}
