import {
  ForbiddenException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import axios, { AxiosError } from 'axios';
import { Repository } from 'typeorm';
import {
  StatusTransactionCallbackENUM,
  StatusTransactionENUM,
} from '../common/constant/status-transaction';
import { Log } from '../log/entity/log.entity';
import { Member } from '../member/entity/member.entity';
import { Order } from '../order/entity/order.entity';
import { TransactionCallbackDTO } from './dto/transaction-callback.dto';

@Injectable()
export class TransactionCallbackService {
  constructor(
    @InjectRepository(Log)
    private logRepository: Repository<Log>,

    @Inject(ConfigService)
    private configService: ConfigService,
  ) {}

  async updateTransaction(payload: TransactionCallbackDTO): Promise<string> {
    const foundTransaction = await this.logRepository.findOne({
      where: { GUID: payload.transaction_id },
      relations: ['order', 'order.member'],
    });
    if (!foundTransaction) {
      throw new NotFoundException(
        `transaction with guid ${payload.transaction_id} not found.`,
      );
    }

    if (payload.status === Number(foundTransaction.status)) {
      throw new ForbiddenException('cannot update same status');
    }
    const member = foundTransaction.order.member;
    const order = foundTransaction.order;

    let status = payload.status;

    await this.logRepository.manager.transaction(async (trx) => {
      let balance = Number(member.balance);
      const total_amount = Number(foundTransaction.total_amount);

      if (payload.status === StatusTransactionCallbackENUM.SUCCESS) {
        status = StatusTransactionCallbackENUM.SUCCESS;

        if (foundTransaction.status === StatusTransactionENUM.FAILED) {
          if (balance < total_amount) {
            throw new ForbiddenException('insufficient balance');
          }
          balance = balance - total_amount;
          await trx.getRepository(Member).update(member.guid, { balance });
        }
      } else {
        balance = balance + total_amount;
        status = StatusTransactionCallbackENUM.FAILED;
        await trx.getRepository(Member).update(member.guid, { balance });
      }

      await trx.getRepository(Order).update(order.GUID, { status });
      await trx.getRepository(Log).update(foundTransaction.GUID, { status });

      foundTransaction.status = status;
    });

    let response;
    let callback_receive_time;
    const url = this.configService.get('URL_TRANSACTION_CALBACK');
    await axios
      .post(url, order)
      .then((res) => {
        callback_receive_time = new Date().toISOString();
        response = res.data;
      })
      .catch((err: AxiosError) => {
        callback_receive_time = new Date().toISOString();
        response = err?.response ? err.response : err.code;
      });

    await this.logRepository.update(foundTransaction.GUID, {
      callback_request: order,
      callback_request_url: url,
      callback_response: response,
      callback_receive_time: callback_receive_time,
    });

    return 'SUCCESS';
  }
}
