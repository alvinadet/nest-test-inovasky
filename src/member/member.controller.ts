import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Put,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { PaginateRequestDTO } from '../common/constant/share.dto';
import { UUIDValidatorDTO } from '../common/constant/validator';
import { PaginateTransformInterceptor } from '../common/utils/interpretor/paginate-transform-interpretor';
import { TransformInterceptor } from '../common/utils/interpretor/transform-interpretor';
import { SubscriberService } from '../subscriber/subscriber.service';
import { MemberCreateSubscriberDTO, MemberUpdateDTO } from './dto/member.dto';
import { MemberService } from './member.service';

@Controller('member')
export class MemberController {
  constructor(
    @Inject(SubscriberService)
    private subscriberService: SubscriberService,

    @Inject(MemberService)
    private memberService: MemberService,
  ) {}

  @UseInterceptors(new PaginateTransformInterceptor())
  @Get()
  async findAll(@Query() query?: PaginateRequestDTO) {
    return await this.memberService.findAll(query);
  }

  @UseInterceptors(new TransformInterceptor())
  @Post()
  async create(@Body() data: MemberCreateSubscriberDTO) {
    this.subscriberService.addMember(data);
    return 'SUCCESS';
  }

  @UseInterceptors(new TransformInterceptor())
  @Put(':guid')
  async update(
    @Param()
    param: UUIDValidatorDTO,
    @Body() data: MemberUpdateDTO,
  ) {
    return await this.memberService.update(param.guid, data);
  }
}
