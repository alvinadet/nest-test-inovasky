import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { MemberType } from '../../member-type/entity/member-type.entity';

@Entity({ name: 'member' })
export class Member {
  @PrimaryGeneratedColumn('uuid', { name: 'guid' })
  guid: string;

  @Column({ name: 'name', type: 'varchar' })
  name: string;

  @Column({ name: 'email', type: 'varchar' })
  email: string;

  @Column({ name: 'password', type: 'varchar' })
  password: string;

  @Column({ name: 'phone_number', type: 'varchar' })
  phone_number: string;

  @Column({ name: 'photo_profile', type: 'varchar' })
  photo_profile: string;

  @Column({ name: 'balance', type: 'decimal' })
  balance: number;

  @Column({ name: 'created_at', nullable: true, default: new Date() })
  created_at: Date;

  @Column({ name: 'updated_at', nullable: true, default: new Date() })
  updated_at: Date;

  @Column({ name: 'member_type_guid' })
  member_type_guid: string;

  @ManyToOne(() => MemberType, (t) => t.GUID)
  @JoinColumn({ name: 'member_type_guid' })
  memberType: MemberType;
}
