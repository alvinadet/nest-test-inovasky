import { MemberDTO } from '../dto/member.dto';
import { Member } from '../entity/member.entity';

export const memberTransformer = (model: Member): MemberDTO => {
  return {
    balance: Number(model.balance || 0),
    created_at: model.created_at,
    email: model.email,
    guid: model.guid,
    member_type_guid: model.memberType as unknown as string,
    name: model.name,
    phone_number: model.phone_number,
    photo_profile: model.photo_profile,
    updated_at: model.updated_at,
  };
};
