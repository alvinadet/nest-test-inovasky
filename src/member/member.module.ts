import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MemberType } from '../member-type/entity/member-type.entity';
import { SubscriberModule } from '../subscriber/subscriber.module';
import { Member } from './entity/member.entity';
import { MemberController } from './member.controller';
import { MemberService } from './member.service';

@Module({
  imports: [SubscriberModule, TypeOrmModule.forFeature([Member, MemberType])],
  providers: [MemberService],
  controllers: [MemberController],
  exports: [MemberService],
})
export class MemberModule {}
