import {
  IsEmail,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

export class MemberCreateSubscriberDTO {
  @IsUUID()
  member_type_guid: string;

  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  phone_number: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  photo_profile: string;

  @IsNumber()
  @IsOptional()
  balance: number;

  @IsString()
  @IsOptional()
  password?: string;
}

export class MemberUpdateDTO {
  @IsUUID()
  member_type_guid: string;

  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  phone_number: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  photo_profile: string;

  @IsNumber()
  @IsOptional()
  balance: number;

  @IsString()
  @IsOptional()
  password?: string;
}
export class MemberDTO {
  guid: string;
  member_type_guid: string;
  name: string;
  phone_number: string;
  email: string;
  photo_profile: string;
  balance: number;
  created_at: Date;
  updated_at: Date;
}
