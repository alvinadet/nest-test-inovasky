import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { PaginateRequestDTO } from '../common/constant/share.dto';
import {
  generatePaginateResponse,
  PaginateDataResponDTO,
} from '../common/utils/interpretor/paginate-transform-interpretor';
import { filterQuery } from '../common/utils/query-utils';
import { MemberType } from '../member-type/entity/member-type.entity';
import { MemberDTO, MemberUpdateDTO } from './dto/member.dto';
import { Member } from './entity/member.entity';
import { memberTransformer } from './transformer/member.transformer';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private memberRepository: Repository<Member>,

    @InjectRepository(MemberType)
    private memberTypeRepository: Repository<MemberType>,
  ) {}

  async findAll(
    query?: PaginateRequestDTO,
  ): Promise<PaginateDataResponDTO<MemberDTO>> {
    const filter = filterQuery(['name', 'email', 'phone_number'], query);

    const [result, total] = await this.memberRepository.findAndCount(
      Object.assign(filter, {
        loadRelationIds: true,
      } as FindManyOptions),
    );

    return {
      result: result.map(memberTransformer),
      paginate: generatePaginateResponse(query, total),
    };
  }

  async update(guid: string, data: MemberUpdateDTO): Promise<MemberDTO> {
    const found = await this.memberRepository.findOne({
      where: { guid },
    });

    if (!found) {
      throw new NotFoundException(`member with guid ${guid} not found.`);
    }

    const memberType = await this.memberTypeRepository.findOne({
      where: { GUID: data.member_type_guid },
    });

    if (!memberType) {
      throw new NotFoundException(
        'member_type_guid not found, please select another member_type_guid',
      );
    }

    await this.memberRepository.update(guid, data);

    const now = new Date();

    return { ...data, created_at: found.created_at, updated_at: now, guid };
  }
}
