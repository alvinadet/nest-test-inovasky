import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { MemberCreateSubscriberDTO } from '../member/dto/member.dto';

@Injectable()
export class SubscriberService {
  constructor(
    @Inject('RABBIT_MQ_MEMBER_SERVICE')
    private readonly subscriberEventService: ClientProxy,
  ) {}

  addMember(data: MemberCreateSubscriberDTO) {
    this.subscriberEventService.emit('add_member', data);
  }
}
