import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { QueueEnum } from '../common/constant/queue';
import { SubscriberService } from './subscriber.service';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: 'RABBIT_MQ_MEMBER_SERVICE',
        imports: [
          ConfigModule.forRoot({
            load: [],
          }),
        ],
        useFactory: (configService: ConfigService) => {
          const user = configService.get('RABBITMQ_USER');
          const password = configService.get('RABBITMQ_PASSWORD');
          const host = configService.get('RABBITMQ_HOST');
          return {
            transport: Transport.RMQ,
            options: {
              urls: [`amqp://${user}:${password}@${host}`],
              queue: QueueEnum.RABBIT_MQ_MEMBER_SERVICE,
              queueOptions: {
                durable: true,
              },
            },
          };
        },
        inject: [ConfigService],
      },
    ]),
  ],
  providers: [SubscriberService],
  exports: [SubscriberService],
})
export class SubscriberModule {}
