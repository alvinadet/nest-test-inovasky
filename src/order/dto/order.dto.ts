import { IsNumber, IsOptional, IsString, IsUUID } from 'class-validator';
import { StatusTransactionENUM } from '../../common/constant/status-transaction';

export class OrderCreateDTO {
  @IsUUID()
  member_guid: string;

  @IsString()
  product_code: string;

  @IsString()
  product_name: string;

  @IsNumber()
  fee: number;

  @IsString()
  @IsOptional()
  description: string;

  @IsNumber()
  amount: number;

  @IsNumber()
  total_amount: number;
}

export class OrderDTO {
  guid: string;
  member_guid: string;
  product_code: string;
  product_name: string;
  fee: number;
  amount: number;
  total_amount: number;
  description: string;
  status: StatusTransactionENUM;
}
