import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import {
  StatusTransactionCallbackENUM,
  StatusTransactionENUM,
} from '../../common/constant/status-transaction';
import { Member } from '../../member/entity/member.entity';

@Entity({
  name: 'order',
})
export class Order {
  @PrimaryGeneratedColumn('uuid', { name: 'guid' })
  GUID: string;

  @Column({ name: 'member_guid' })
  member_guid: string;

  @ManyToOne(() => Member, (t) => t.guid)
  @JoinColumn({ name: 'member_guid' })
  member: Member;

  @Column({ name: 'total_amount', type: 'decimal' })
  total_amount: number;

  @Column({ name: 'status', type: 'smallint' })
  status: StatusTransactionENUM | StatusTransactionCallbackENUM;

  @Column({ name: 'created_at', nullable: true, default: new Date() })
  created_at: Date;

  @Column({ name: 'updated_at', nullable: true, default: new Date() })
  updated_at: Date;
}
