import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { StatusTransactionENUM } from '../common/constant/status-transaction';
import { Log } from '../log/entity/log.entity';
import { Member } from '../member/entity/member.entity';
import { OrderCreateDTO, OrderDTO } from './dto/order.dto';
import { Order } from './entity/order.entity';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,

    @InjectRepository(Member)
    private memberRepository: Repository<Member>,
  ) {}

  async create(payload: OrderCreateDTO) {
    const {
      member_guid,
      amount,
      fee,
      product_code,
      product_name,
      total_amount,
      description,
    } = payload;

    const member = await this.memberRepository.findOne({
      where: { guid: member_guid },
    });

    if (!member) {
      throw new NotFoundException(
        `Member with guid ${member_guid} isn't found!`,
      );
    }

    const order = await this.orderRepository.manager.transaction(
      async (trx): Promise<OrderDTO> => {
        let status: StatusTransactionENUM = StatusTransactionENUM.PENDING;

        const memberBalance = Number(member.balance);
        if (memberBalance < payload.total_amount) {
          status = StatusTransactionENUM.FAILED;
        }

        const orderRepo = trx.getRepository(Order);
        // Mapping data create order
        const orderValue = orderRepo.create({
          total_amount,
          member_guid,
          status,
        });

        //   insert Order
        const orderRes = await orderRepo.save(orderValue);

        const logRepo = trx.getRepository(Log);

        const logValue = logRepo.create({
          amount,
          fee,
          order_guid: orderRes.GUID,
          description,
          status,
          total_amount,
          product_code,
          product_name,
        });

        const logRes = await logRepo.save(logValue);

        if (status === StatusTransactionENUM.PENDING) {
          const balance = memberBalance - payload.total_amount;
          await trx
            .getRepository(Member)
            .update(payload.member_guid, { balance });
        }

        return {
          amount: logRes.amount,
          description: logRes.description,
          fee: logRes.fee,
          guid: orderRes.GUID,
          member_guid: orderRes.member_guid,
          product_code: logRes.product_code,
          product_name: logRes.product_name,
          status: Number(logRes.status),
          total_amount: logRes.total_amount,
        };
      },
    );

    if (order.status === StatusTransactionENUM.FAILED) {
      throw new ForbiddenException('insufficient balance');
    }

    return order;
  }
}
