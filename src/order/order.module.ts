import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Log } from '../log/entity/log.entity';
import { Member } from '../member/entity/member.entity';
import { Order } from './entity/order.entity';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';

@Module({
  imports: [TypeOrmModule.forFeature([Order, Log, Member])],
  providers: [OrderService],
  controllers: [OrderController],
})
export class OrderModule {}
