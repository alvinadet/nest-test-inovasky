import {
  Body,
  Controller,
  Inject,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { TransformInterceptor } from '../common/utils/interpretor/transform-interpretor';
import { OrderCreateDTO } from './dto/order.dto';
import { OrderService } from './order.service';
@UseInterceptors(new TransformInterceptor())
@Controller('order')
export class OrderController {
  constructor(
    @Inject(OrderService)
    private orderService: OrderService,
  ) {}

  @Post()
  async createOrder(@Body() payload: OrderCreateDTO) {
    return await this.orderService.create(payload);
  }
}
