import { INestApplication, Logger } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { Connection, getConnection } from 'typeorm';
import { AppModule } from '../src/app.module';
import { StatusTransactionENUM } from '../src/common/constant/status-transaction';
import { Response } from '../src/common/utils/interpretor/transform-interpretor';
import { members } from '../src/database/seeders/member-type/data';
import { Seeder } from '../src/database/seeders/seeder.service';
import { Log } from '../src/log/entity/log.entity';
import { Member } from '../src/member/entity/member.entity';
import { OrderCreateDTO, OrderDTO } from '../src/order/dto/order.dto';
import { TransactionCallbackDTO } from '../src/transaction-callback/dto/transaction-callback.dto';

describe('Order', () => {
  let app: INestApplication;

  let memberBalance = 0;
  let connection: Connection;

  let transaction: Log;

  const orderCreate: Partial<OrderCreateDTO> = {
    amount: 500,
    description: '',
    fee: 0,
    member_guid: null,
    product_code: 'AHA7',
    product_name: 'SAPU',
    total_amount: 500,
  };

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    connection = getConnection();

    await connection.query('TRUNCATE public.log RESTART IDENTITY CASCADE');
    await connection.query('TRUNCATE public.order RESTART IDENTITY CASCADE');
    await connection.query('TRUNCATE public.member RESTART IDENTITY CASCADE');
    await connection.query(
      'TRUNCATE public.member_type RESTART IDENTITY CASCADE',
    );

    const seed = app.get(Seeder);
    const logger = app.get(Logger);

    await seed
      .seed()
      .then(() => {
        logger.debug('Seeding complete!');
      })
      .catch((error) => {
        logger.error('Seeding failed!');
        throw error;
      });

    const membersRes = await connection.getRepository(Member).findOne({
      where: {
        name: members[1].name,
      },
    });

    orderCreate.member_guid = membersRes.guid;

    memberBalance = Number(membersRes.balance);
  });

  afterAll(async () => {
    await app.close();
  });

  it('/order (POST) SUCCESS', async () => {
    return request(app.getHttpServer())
      .post(`/order`)
      .send(orderCreate)
      .then(async (result) => {
        const {
          status,
          success,
          message,
          data: orderResponse,
        } = result.body as Response<OrderDTO>;

        expect(status).toBe(201);
        expect(success).toBe(true);
        expect(message).toEqual('OK');

        expect(orderResponse).toMatchObject<OrderDTO>({
          amount: orderCreate.amount,
          description: orderCreate.description,
          fee: orderCreate.fee,
          guid: expect.any(String),
          member_guid: orderCreate.member_guid,
          product_code: orderCreate.product_code,
          product_name: orderCreate.product_name,
          status: StatusTransactionENUM.PENDING,
          total_amount: orderCreate.total_amount,
        });

        // Make sure status transaction is pending

        const transactionRes = await connection.getRepository(Log).findOne({
          where: {
            order_guid: orderResponse.guid,
          },
        });

        expect(transactionRes.status).toEqual(StatusTransactionENUM.PENDING);

        // Make sure balance cutting

        const memberNewRes = await connection
          .getRepository(Member)
          .findOne({ where: { guid: orderCreate.member_guid } });

        const balaceExpect = memberBalance - orderCreate.total_amount;

        expect(Number(memberNewRes.balance)).toEqual(balaceExpect);

        transaction = transactionRes;
      });
  });

  it('/transaction-callback (PUT) TRANSACTION CALBACK STATUS 1', async () => {
    return request(app.getHttpServer())
      .post(`/transaction-callback`)
      .send({
        status: StatusTransactionENUM.SUCCESS,
        transaction_id: transaction.GUID,
      })
      .then(async (result) => {
        const { status, success, message } =
          result.body as Response<TransactionCallbackDTO>;

        expect(status).toBe(201);
        expect(success).toBe(true);
        expect(message).toEqual('OK');

        // Make sure status transaction is pending

        const transactionRes = await connection.getRepository(Log).findOne({
          where: {
            GUID: transaction.GUID,
          },
        });

        expect(transactionRes.status).toEqual(StatusTransactionENUM.SUCCESS);
      });
  });

  it('/transaction-callback (PUT) TRANSACTION CALLBCK STATUS 2', async () => {
    return request(app.getHttpServer())
      .post(`/transaction-callback`)
      .send({
        status: StatusTransactionENUM.FAILED,
        transaction_id: transaction.GUID,
      })
      .then(async (result) => {
        const { status, success, message } =
          result.body as Response<TransactionCallbackDTO>;

        expect(status).toBe(201);
        expect(success).toBe(true);
        expect(message).toEqual('OK');

        // Make sure status transaction is pending

        const transactionRes = await connection.getRepository(Log).findOne({
          where: {
            GUID: transaction.GUID,
          },
        });

        expect(transactionRes.status).toEqual(StatusTransactionENUM.FAILED);

        const memberNewRes = await connection
          .getRepository(Member)
          .findOne({ where: { guid: orderCreate.member_guid } });

        const balaceExpect = memberBalance;
        expect(Number(memberNewRes.balance)).toEqual(balaceExpect);
      });
  });
});
