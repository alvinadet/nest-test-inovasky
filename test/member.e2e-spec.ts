import { INestApplication, Logger } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { getConnection } from 'typeorm';
import { AppModule } from '../src/app.module';
import { Response } from '../src/common/utils/interpretor/transform-interpretor';
import { members } from '../src/database/seeders/member-type/data';
import { Seeder } from '../src/database/seeders/seeder.service';
import { MemberDTO, MemberUpdateDTO } from '../src/member/dto/member.dto';

describe('Member (e2e)', () => {
  let member: MemberDTO;
  let app: INestApplication;
  let connection;

  const updateValueMember: Partial<MemberUpdateDTO> = {
    balance: 5000,
    email: 'alvin@mail.com',
    name: 'zooo',
    phone_number: '0099909343',
    photo_profile: 'profule',
    password: 'ahaide',
    member_type_guid: null,
  };

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    connection = getConnection();

    await connection.query('TRUNCATE public.log RESTART IDENTITY CASCADE');
    await connection.query('TRUNCATE public.order RESTART IDENTITY CASCADE');
    await connection.query('TRUNCATE public.member RESTART IDENTITY CASCADE');
    await connection.query(
      'TRUNCATE public.member_type RESTART IDENTITY CASCADE',
    );

    const seed = app.get(Seeder);
    const logger = app.get(Logger);
    await seed
      .seed()
      .then(() => {
        logger.debug('Seeding complete!');
      })
      .catch((error) => {
        logger.error('Seeding failed!');
        throw error;
      });
  });

  afterAll(async () => {
    await app.close();
  });

  it('/MEMBER (GET) SUCCESS', async () =>
    request(app.getHttpServer())
      .get('/member')
      .then((result) => {
        const { status, success, message, data } = result.body as Response<
          MemberDTO[]
        >;

        expect(status).toBe(200);
        expect(success).toBe(true);
        expect(message).toEqual('OK');

        expect(data.length).toEqual(members.length);

        const memberRes = data[0];
        const expectMember = members.find((l) => l.name === memberRes.name);

        expect(memberRes).toMatchObject<MemberDTO>({
          balance: expectMember.balance,
          email: expectMember.email,
          guid: expect.any(String),
          created_at: expect.any(String),
          member_type_guid: expect.any(String),
          name: expectMember.name,
          phone_number: expectMember.phone_number,
          photo_profile: expectMember.photo_profile,
          updated_at: expect.any(String),
        });

        member = memberRes;

        updateValueMember.member_type_guid = memberRes.member_type_guid;
      }));

  it('/MEMBER (PUT) SUCCESS', async () => {
    return request(app.getHttpServer())
      .put(`/member/${member.guid}`)
      .send(updateValueMember)
      .then(async (result) => {
        const { status, success, message, data } =
          result.body as Response<MemberDTO>;

        expect(status).toBe(200);
        expect(success).toBe(true);
        expect(message).toEqual('OK');

        const responseMembers: MemberDTO[] = (
          await request(app.getHttpServer()).get('/member')
        ).body.data;

        const resMember = responseMembers.find((l) => l.name === data.name);

        expect(resMember).toMatchObject<MemberDTO>({
          balance: updateValueMember.balance,
          email: updateValueMember.email,
          guid: expect.any(String),
          created_at: expect.any(String),
          member_type_guid: updateValueMember.member_type_guid,
          name: updateValueMember.name,
          phone_number: updateValueMember.phone_number,
          photo_profile: updateValueMember.photo_profile,
          updated_at: expect.any(String),
        });
      });
  });

  it('/MEMBER (PUT) FAILED BECAUSE PAYLOAD WRONG', async () => {
    return request(app.getHttpServer())
      .put(`/member/${member.guid}`)
      .send({ foo: 'coba' })
      .then(async (result) => {
        expect(result.status).toBe(404);
      });
  });
});
