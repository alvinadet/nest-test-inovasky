import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class order1677747042361 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'order',
        columns: [
          {
            name: 'guid',
            type: 'uuid',
            isPrimary: true,
            isUnique: true,
            generationStrategy: 'uuid',
            default: `uuid_generate_v4()`,
          },
          { name: 'status', type: 'smallint', isNullable: true },
          { name: 'total_amount', type: 'numeric', isNullable: true },
          { name: 'member_guid', type: 'uuid' },
          {
            name: 'updated_at',
            type: 'timestamptz',
            default: 'now()',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamptz',
            default: 'now()',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'order',
      new TableForeignKey({
        columnNames: ['member_guid'],
        referencedColumnNames: ['guid'],
        referencedTableName: 'member',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('order');
  }
}
