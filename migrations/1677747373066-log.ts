import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class log1677747373066 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'log',
        columns: [
          {
            name: 'guid',
            type: 'uuid',
            isPrimary: true,
            isUnique: true,
            generationStrategy: 'uuid',
            default: `uuid_generate_v4()`,
          },
          { name: 'product_code', type: 'varchar', isNullable: true },
          { name: 'product_name', type: 'varchar', isNullable: true },
          { name: 'amount', type: 'numeric', isNullable: true, default: 0 },
          { name: 'fee', type: 'numeric', isNullable: true, default: 0 },
          { name: 'total_amount', type: 'numeric' },
          { name: 'description', type: 'text', isNullable: true },
          { name: 'status', type: 'smallint', isNullable: true },
          { name: 'order_guid', type: 'uuid' },
          { name: 'callback_request_url', type: 'text', isNullable: true },
          { name: 'callback_request', type: 'jsonb', isNullable: true },
          { name: 'callback_response', type: 'text', isNullable: true },
          {
            name: 'callback_receive_time',
            type: 'timestamptz',
            isNullable: true,
          },

          {
            name: 'updated_at',
            type: 'timestamptz',
            default: 'now()',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamptz',
            default: 'now()',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'log',
      new TableForeignKey({
        columnNames: ['order_guid'],
        referencedColumnNames: ['guid'],
        referencedTableName: 'order',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('log');
  }
}
