import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class member1677746134695 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'member',
        columns: [
          {
            name: 'guid',
            type: 'uuid',
            isPrimary: true,
            isUnique: true,
            generationStrategy: 'uuid',
            default: `uuid_generate_v4()`,
          },
          { name: 'name', type: 'varchar', isNullable: true },
          { name: 'email', type: 'varchar', isNullable: true },
          { name: 'password', type: 'varchar', isNullable: true },
          { name: 'phone_number', type: 'varchar', isNullable: true },
          { name: 'photo_profile', type: 'varchar', isNullable: true },
          { name: 'balance', type: 'numeric', isNullable: true },
          { name: 'member_type_guid', type: 'uuid' },
          {
            name: 'updated_at',
            type: 'timestamptz',
            default: 'now()',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'timestamptz',
            default: 'now()',
            isNullable: true,
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'member',
      new TableForeignKey({
        columnNames: ['member_type_guid'],
        referencedColumnNames: ['guid'],
        referencedTableName: 'member_type',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('member');
  }
}
