// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

const config = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  migrationsTableName: 'migration',
  migrations: ['migrations/*.ts'],
  synchronize: false,
  cli: {
    entitiesDir: [__dirname + '/../**/*.entity{.ts,.js}'],
    migrationsDir: 'migrations',
  },
};

export = config;
