## Requitment
- make sure you have been already running Postgresql and rabbitmq to running this application
- please follow step by step to run this application.

## Installation

```bash
$ yarn install
```

## Running the app

````migration data
$ yarn db:run

``` seeding data
$ yarn seed 
for init first data. this commnad is optional.

```bash
# watch mode
$ yarn run start:dev

# development
$ yarn run start

## build application
$ yarn build

# production mode
$ yarn run start:prod
````

## Test
### Note

- If wanna testing you have to change to different database in .env.test

```bash


# e2e tests
$ yarn run test:e2e
```
